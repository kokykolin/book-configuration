import _ from "lodash";
import {validationResult} from "express-validator";
import LedgeringAccountsDao from "../dao/ledgering-accounts.dao";
import LedgeringFramesDao from "../dao/ledgering-frames.dao";

const WEBSOCKET_BATCH_SIZE = 50;

export default class LedgeringController {

    /**
     * @deprecated Use LedgeringController.frames(...)
     */
    static framesOld(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        LedgeringController.getFrames(req.body)
            .then(result => res.json(result))
            .catch(next);
    }

    static frames(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        LedgeringFramesDao.getFrames(req.body, async cursor => await cursor.toArray())
            .then(result => res.json(result))
            .catch(next);
    }

    static framesStreamed(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        LedgeringFramesDao.getFrames(req.body, cursor => cursor).then(stream => {
            stream.on('error', next);
            stream.on('data', (data) => {
                res.write(`${JSON.stringify(data)}\n`)
            });
            stream.on('end', () => {
                res.end()
            });
        }).catch(next)
    }

    static framesWs(ws, req) {
        ws.on('message', (msg) => {
            const msgObject = JSON.parse(msg);
            const {reqId} = msgObject;
            LedgeringFramesDao.getFrames(_.omit(msgObject, 'reqId'), cursor => cursor)
                .then(stream => {
                    const batchSize = WEBSOCKET_BATCH_SIZE;
                    let counter = 0;
                    let buffer = [];
                    stream.on('error', (error) => {
                        ws.send(`${JSON.stringify(error)}`)
                    });
                    stream.on('data', (data) => {
                        if (counter < batchSize) {
                            buffer = [...buffer, data];
                            counter++;
                        } else {
                            ws.send(`${JSON.stringify({data: buffer, reqId})}`);
                            buffer = [data];
                            counter = 0;
                        }
                    });
                    stream.on('end', () => {
                        if (buffer.length) {
                            ws.send(`${JSON.stringify({data: buffer, reqId})}`);
                        }
                    });
                }).catch((error) => {
                ws.send(`${JSON.stringify(error)}`)
            });
        })
    }

    static frame(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        const data = _.isEqual(req.body, {} )? req.query : req.body;
        LedgeringFramesDao.getFrame(data.name)
            .then(result => res.json(result))
            .catch(next);
    }

    static accounts(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        LedgeringAccountsDao.getAccounts(req.body)
            .then(result => res.json(result))
            .catch(next);
    }

    static filters(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        const data = _.isEqual(req.body, {} )? req.query : req.body;
        LedgeringAccountsDao.getFilters(data)
            .then(result => res.json(result))
            .catch(next);
    }

    static getFrames(filter) {
        if (_.isEmpty(filter.accounts) && _.isEmpty(filter.accountOwners) && _.isEmpty(filter.schemes)) {
            return LedgeringFramesDao.getFrames(filter);
        } else {
            return LedgeringAccountsDao.getFrames(filter);
        }
    }
}
