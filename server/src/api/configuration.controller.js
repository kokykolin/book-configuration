import {Transform} from 'stream'
import JSONStream from 'JSONStream';
import xmlToJson from 'xml-to-json-stream';
import _ from 'lodash';
import AccountingRuleGroupsDao from "../dao/accounting-rule-groups.dao";
import IgnoringRuleGroupsDao from "../dao/ignoring-rule-groups.dao";
import IgnoringFramesDAO from "../dao/ingnoring-frames.dao";
import LedgeringFramesDao from "../dao/ledgering-frames.dao";
import AccountingFramesDao from "../dao/accounting-frames.dao";
import LedgeringRuleGroupsDao from "../dao/ledgering-rule-groups.dao";
import LedgeringProviderGroupsDao from "../dao/ledgering-provider-groups.dao";
import AccountingProviderGroupsDao from "../dao/accounting-provider-groups.dao";
import LedgeringAccountsDao from "../dao/ledgering-accounts.dao";
import {frameMapper, providerGroupMapper, ruleGroupMapper} from "../utils/xml2json.helper";

export default class ConfigurationController {

    static upload(req, res, next) {
        req.pipe(req.busboy);

        req.busboy.on('file', (field, file, filename) => {
            const xmlToJsonStream = xmlToJson().createStream();
            xmlToJsonStream.on('error', error => next(error));
            const objectWriter = new ObjectWriter();
            objectWriter.on('error', error => next(error));

            file.pipe(new XmlCommentRemover())
                .pipe(xmlToJsonStream)
                .pipe(JSONStream.parse(['configuration', {emitKey: true}]))
                .pipe(objectWriter)
                .pipe(res);
        });
    }

    static delete(req, res, next) {
        Promise.all([
            AccountingRuleGroupsDao.deleteAll(),
            AccountingProviderGroupsDao.deleteAll(),
            AccountingFramesDao.deleteAll(),
            LedgeringAccountsDao.deleteAll(),
            LedgeringRuleGroupsDao.deleteAll(),
            LedgeringProviderGroupsDao.deleteAll(),
            LedgeringFramesDao.deleteAll(),
            IgnoringRuleGroupsDao.deleteAll(),
            IgnoringFramesDAO.deleteAll()
        ]).then((results) => {
            const result = _.assignIn({success: true}, ...results);
            res.json(result);
        }).catch(error => next(error));
    }
}

class ObjectWriter extends Transform {
    constructor() {
        super({objectMode: true});
        this.total = {};
        this.promises = [];
        this.accounts = [];
    }

    _flush(done) {
        Promise.all(this.promises)
            .then((results) => {
                    this.total = _.assignIn(this.total, ...results);
                    //this.push(JSON.stringify(result));
                }
                ).then(() =>
                   this.ledgeringFramesCallback(this.accounts)
             ).then((result) => {
                   const response = _.assignIn({success: true, ...this.total}, result);
                   this.push(JSON.stringify(response));
                }
             )
            .then(done)
            .catch(error => done(error, null));
    }

    _transform(chunk, encoding, done) {
        let data;
        switch (chunk.key) {
            case 'accountingRuleGroups':
                this.promises.push(AccountingRuleGroupsDao.insertRuleGroups(
                    this._toArray(chunk.value, 'group').map(ruleGroupMapper)));
                break;
            case 'accountingProviderGroups':
                this.promises.push(AccountingProviderGroupsDao.insertProviderGroups(
                    this._toArray(chunk.value, 'group').map(providerGroupMapper)));
                break;
            case 'accountingFrames':
                this.promises.push(AccountingFramesDao.insertFrames(
                    this._toArray(chunk.value, 'frame').map(frameMapper)));
                break;
            case 'ledgeringAccounts':
                this.accounts = this._toArray(chunk.value, 'account');
                this.promises.push(LedgeringAccountsDao.insertAccounts(this.accounts));
                break;
            case 'ledgeringRuleGroups':
                this.promises.push(LedgeringRuleGroupsDao.insertRuleGroups(
                    this._toArray(chunk.value, 'group').map(ruleGroupMapper)));
                break;
            case 'ledgeringProviderGroups':
                this.promises.push(LedgeringProviderGroupsDao.insertProviderGroups(
                    this._toArray(chunk.value, 'group').map(providerGroupMapper)));
                break;
            case 'ledgeringFrames':
                this.ledgeringFramesCallback = async (accounts) => {
                    const frames = this._toArray(chunk.value, 'frame').map(frameMapper);
                    frames.map(frame => frame.segments)
                        .reduce((segmentsAll, segments) => segmentsAll.concat(segments), [])
                        .map(segment => {
                            segment.ledgerAccount = accounts.find(account => account.name === segment.ledgerAccount);
                            delete segment.ledgerAccount['_id'];
                        });
                    return LedgeringFramesDao.insertFrames(frames);
                };
                break;
            case 'ignoringRuleGroups':
                this.promises.push(IgnoringRuleGroupsDao.insertRuleGroups(
                    this._toArray(chunk.value, 'group').map(ruleGroupMapper)));
                break;
            case 'ignoringFrames':
                this.promises.push(IgnoringFramesDAO.insertFrames(
                    this._toArray(chunk.value, 'frame').map(frameMapper)));
                break;
        }
        done();
    }

    _toArray(data, field) {
        return Array.isArray(data[field]) ? data[field] : [data[field]];
    }

}

class XmlCommentRemover extends Transform {
    constructor() {
        super();
        this.buffer = '';
    }

    _transform(chunk, encoding, done) {
        const withoutComments = this._removeComments(chunk.toString());
        if (withoutComments.includes('<!--')) {
            this.buffer += withoutComments;
        } else if (withoutComments.includes('-->')) {
            this.buffer += withoutComments;
            this.push(this._removeComments(this.buffer));
            this.buffer = '';
        } else if (this.buffer) {
            // do nothing
        } else {
            this.push(withoutComments);
        }
        done();
    }

    _removeComments(text) {
        return text.replace(/<!--[\s\S]*?-->/g, '');
    }
}