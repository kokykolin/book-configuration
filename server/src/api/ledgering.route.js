import express, { Router } from "express";
import expressWs from 'express-ws';
import {check} from "express-validator";
import LedgeringCtrl from "./ledgering.controller"

const router = new Router();
expressWs(router);

const FRAMES_INPUT_SCHEME = [
    check('accounts').isArray({min: 0, max: 10}).optional(true),
    check('accountOwners').isArray({min: 0}).optional(true),
    check('schemes').isArray({min: 0, max: 2}).optional(true),
    check('sides').isArray({min: 0, max: 2}).optional(true)
]

router.route("/framesStream").get(FRAMES_INPUT_SCHEME, LedgeringCtrl.framesStreamed);
router.route("/frames").get(FRAMES_INPUT_SCHEME, LedgeringCtrl.frames);
/**
 * @deprecated Use route("/frames")
 */
router.route("/framesOld").get(FRAMES_INPUT_SCHEME, LedgeringCtrl.framesOld);
router.route("/frame").get(
    [check('name').isString()],
    LedgeringCtrl.frame);
router.route("/accounts").get(
    [
        check('search').isString().optional(true),
        check('accountOwners').isArray({min: 0}).optional(true),
        check('schemes').isArray({min: 0}).optional(true)
    ], LedgeringCtrl.accounts);
router.route("/filters").get(
    [
        check('accounts').isArray({min: 0, max: 10}).optional(true),
        check('accountOwners').isArray({min: 0}).optional(true),
        check('schemes').isArray({min: 0}).optional(true)
    ], LedgeringCtrl.filters);

router.ws("/frames", LedgeringCtrl.framesWs);

export default router;