import { Router } from "express"
import ConfigurationCtrl from "./configuration.controller"


const router = new Router();

router.route("/upload").post(ConfigurationCtrl.upload);
router.route("/delete").delete(ConfigurationCtrl.delete);

export default router;