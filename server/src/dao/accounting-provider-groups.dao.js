let accountingProviderGroups

export default class AccountingProviderGroupsDao {

    static async injectDB(conn) {
        if (accountingProviderGroups) {
            return
        }
        try {
            accountingProviderGroups = await conn.db(process.env.DB_NS).collection("accountingProviderGroups");
            await accountingProviderGroups.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertProviderGroups(providerGroups) {
        try {
            return await accountingProviderGroups.insertMany(providerGroups)
                .then(result => {
                    return {accountingProviderGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post accountingProviderGroups: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await accountingProviderGroups.deleteMany()
                .then(result => {
                    return {accountingProviderGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete accountingProviderGroups: ${e}`);
            throw e;
        }
    }

}