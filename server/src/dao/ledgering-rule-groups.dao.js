let ledgeringRuleGroups

export default class LedgeringRuleGroupsDao {

    static async injectDB(conn) {
        if (ledgeringRuleGroups) {
            return
        }
        try {
            ledgeringRuleGroups = await conn.db(process.env.DB_NS).collection("ledgeringRuleGroups");
            await ledgeringRuleGroups.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertRuleGroups(ruleGroups) {
        try {
            return await ledgeringRuleGroups.insertMany(ruleGroups)
                .then(result => {
                    return {ledgeringRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ledgeringRuleGroups: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ledgeringRuleGroups.deleteMany()
                .then(result => {
                    return {ledgeringRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ledgeringRuleGroups: ${e}`);
            throw e;
        }
    }

}