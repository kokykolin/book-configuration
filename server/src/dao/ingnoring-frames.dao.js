let ignoringFrames

export default class IgnoringFramesDAO {

    static async injectDB(conn) {
        if (ignoringFrames) {
            return
        }
        try {
            ignoringFrames = await conn.db(process.env.DB_NS).collection("ignoringFrames");
            await ignoringFrames.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertFrames(frames) {
        try {
            return await ignoringFrames.insertMany(frames)
                .then(result => {
                    return {ignoringFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ignoringFrames: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ignoringFrames.deleteMany()
                .then(result => {
                    return {ignoringFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ignoringFrames: ${e}`);
            throw e;
        }
    }
}