let accountingFrames

export default class AccountingFramesDao {

    static async injectDB(conn) {
        if (accountingFrames) {
            return
        }
        try {
            accountingFrames = await conn.db(process.env.DB_NS).collection("accountingFrames");
            await accountingFrames.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertFrames(frames) {
        try {
            return await accountingFrames.insertMany(frames)
                .then(result => {
                    return {accountingFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post accountingFrames: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await accountingFrames.deleteMany()
                .then(result => {
                    return {accountingFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete accountingFrames: ${e}`);
            throw e;
        }
    }

}