import _ from "lodash";

let ledgeringAccounts;

export default class LedgeringAccountsDao {

    static async injectDB(conn) {
        if (ledgeringAccounts) {
            return
        }
        try {
            ledgeringAccounts = await conn.db(process.env.DB_NS).collection("ledgeringAccounts");
            await ledgeringAccounts.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertAccounts(accounts) {
        try {
            return await ledgeringAccounts.insertMany(accounts)
                .then(result => {
                    return {ledgeringAccounts: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ledgeringAccounts: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ledgeringAccounts.deleteMany()
                .then(result => {
                    return {ledgeringAccounts: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ledgeringAccounts: ${e}`);
            throw e;
        }
    }

    static async getFilters(filter) {
        const accountFilter = _.isEmpty(filter.accounts) ? true : {$in: ["$account", filter.accounts]};
        const accountOwnerFilter = _.isEmpty(filter.accountOwners) ? true : {$in: ["$accountOwner", filter.accountOwners]};
        const accountSchemeFilter = _.isEmpty(filter.schemes) ? true : {$in: ["$scheme", filter.schemes]};

        try {
            return await ledgeringAccounts.aggregate([{
                $facet: {
                    accountOwners: [{
                        $project: {
                            _id: 0,
                            enabled: {
                                $cond: {
                                    if: {
                                        $and: [
                                            accountFilter,
                                            accountSchemeFilter
                                        ]
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                            accountOwner: "$accountOwner"
                        }
                    }, {
                        $group: {
                            _id: "$accountOwner",
                            enabled: {
                                $push: "$enabled"
                            }
                        }
                    }, {
                            $project: {
                                _id: 0,
                                value: "$_id",
                                enabled: {$anyElementTrue: "$enabled"}
                            }
                        }
                        , {
                        $match: {
                            value: {
                                "$ne": null

                            }
                        }
                    }],
                    schemes: [{
                        $project: {
                            _id: 0,
                            enabled: {
                                $cond: {
                                    if: {
                                        $and: [
                                            accountFilter,
                                            accountOwnerFilter
                                        ]
                                    },
                                    then: true,
                                    else: false
                                }
                            },
                            scheme: "$scheme"
                        }
                    }, {
                        $group: {
                            _id: "$scheme",
                            enabled: {
                                $push: "$enabled"
                            }
                        }
                    }, {
                        $project: {
                            _id: 0,
                            value: "$_id",
                            enabled: {$anyElementTrue: "$enabled"}
                        }
                    }]
                }
            }]).next();
        } catch (e) {
            console.error(`Unable to get ledgeringAccountsFilter(${filter}*): ${e}`);
            throw e;
        }
    }

    static async getAccounts(filter) {
        const accountFilter = !filter.search ? {} : {account: {$regex: `^${filter.search}`, $options: "i"}};
        const accountOwnerFilter = _.isEmpty(filter.accountOwners) ? {} : {$expr: {$in: ["$accountOwner", filter.accountOwners]}};
        const accountSchemeFilter = _.isEmpty(filter.schemes) ? {} : {$expr: {$in: ["$scheme", filter.schemes]}};

        try {
            return await ledgeringAccounts.aggregate([
                {$match: {$and: [accountFilter, accountOwnerFilter, accountSchemeFilter]}},
                {
                    $group: {
                        _id: "$account",
                        schemes: {
                            $addToSet: "$scheme"
                        },
                        accountOwners: {
                            $addToSet: "$accountOwner"
                        }
                    }
                },
                {$sort: {"_id": 1}},
                {$project: {_id: 0, "account": "$_id", "schemes": "$schemes", "accountOwners": "$accountOwners"}}
            ]).toArray();
        } catch (e) {
            console.error(`Unable to get ledgeringAccounts(${filter}*): ${e}`);
            throw e;
        }
    }

    /**
     * @deprecated Use LedgeringFramesDao.getFrames
     */
    static async getFrames(filter) {
        const accountFilter = _.isEmpty(filter.accounts) ? true : {$in: ['$account', filter.accounts]};
        const accountOwnerFilter = _.isEmpty(filter.accountOwners) ? true : {$in: ['$accountOwner', filter.accountOwners]};
        const schemeFilter = _.isEmpty(filter.schemes) ? true : {$in: ['$scheme', filter.schemes]};

        const andMatchFilter = [accountFilter, accountOwnerFilter, schemeFilter];

        const sideFilter = _.isEmpty(filter.sides) ? true : {
            $in: [
                '$segments.side', filter.sides
            ]
        };

        try {
            const aggregation = await ledgeringAccounts.aggregate([
                {
                    $match: {
                        $expr: {
                            $and: andMatchFilter
                        }
                    }
                }, {
                    $lookup: {
                        from: 'ledgeringFrames',
                        let: {'accountName': '$name'},
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: [
                                            '$$accountName', '$segments.ledgerAccount.name'
                                        ]
                                    }
                                }
                            }, {
                                $addFields: {'segmentsOriginal': '$segments'}
                            }, {
                                $unwind: '$segments'
                            }, {
                                $match: {
                                    $and: [
                                        {
                                            $expr: {
                                                $eq: ['$$accountName', '$segments.ledgerAccount.name']
                                            }
                                        }, {
                                            $expr: sideFilter
                                        }
                                    ]
                                }
                            }
                        ],
                        as: 'ledgeringFrames'
                    }
                }, {
                    $unwind: {
                        path: '$ledgeringFrames',
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $unwind: {
                        path: '$segments',
                        preserveNullAndEmptyArrays: true
                    }
                }, {
                    $group: {
                        '_id': {
                            '_id': '$ledgeringFrames._id',
                            'name': '$ledgeringFrames.name',
                            'ruleGroups': '$ledgeringFrames.ruleGroups',
                            'providerGroups': '$ledgeringFrames.providerGroups',
                            'segments': '$ledgeringFrames.segmentsOriginal'
                        }
                    }
                }, {
                    $project: {
                        '_id': 0,
                        'name': '$_id.name',
                        'ruleGroups': '$_id.ruleGroups',
                        'providerGroups': '$_id.providerGroups',
                        'segments': '$_id.segments'
                    }
                }, {
                    $sort: {
                        'name': 1
                    }
                }
            ]);
            return await aggregation.toArray();
        } catch (e) {
            console.error(`Unable to get ledgeringFrames: ${e}`);
            throw e;
        }
    }

}