import _ from 'lodash';

let ledgeringFrames;

export default class LedgeringFramesDao {

    static async injectDB(conn) {
        if (ledgeringFrames) {
            return
        }
        try {
            ledgeringFrames = await conn.db(process.env.DB_NS).collection("ledgeringFrames");
            await ledgeringFrames.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertFrames(frames) {
        try {
            return await ledgeringFrames.insertMany(frames)
                .then(result => {
                    return {ledgeringFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ledgeringFrames: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ledgeringFrames.deleteMany()
                .then(result => {
                    return {ledgeringFrames: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ledgeringFrames: ${e}`);
            throw e;
        }
    }

    static async getFrames(filter, process) {

        const accountSelector = _.isEmpty(filter.accounts) ? {} : {'ledgerAccount.account': {$in: filter.accounts}};
        const accountOwnerSelector = _.isEmpty(filter.accountOwners) ? {} : {'ledgerAccount.accountOwner': {$in: filter.accountOwners}};
        const schemesSelector = _.isEmpty(filter.schemes) ? {} : {'ledgerAccount.scheme': {$in: filter.schemes}};
        const sideSelector = _.isEmpty(filter.sides) ? {} : {side: {$in: filter.sides}};

        const elemMatchSelector = {
            segments: {
                $elemMatch: {
                    ...accountSelector,
                    ...accountOwnerSelector,
                    ...schemesSelector,
                    ...sideSelector
                }
            }
        };
        try {
            return process(ledgeringFrames.find(elemMatchSelector, {projection: {_id: 0}}).sort({name: 1}));
        } catch (e) {
            console.error(`Unable to get ledgeringFrames: ${e}`);
            throw e;
        }
    }

    static async getFrame(name) {
        try {
            const aggregation = await ledgeringFrames.aggregate(
                [{
                    $match: {
                        $expr: {$eq: ["$name", name]}
                    }
                }/*, {$unwind: {
                        path: "$segments",
                        preserveNullAndEmptyArrays: true
                    }}, {$lookup: {
                        from: 'ledgeringAccounts',
                        let: {"accountName" : "$segments.ledgerAccount"},
                        pipeline: [
                            {
                                $match: {$expr: {$eq: ["$$accountName", "$name"]}}
                            },{
                                $project: {
                                    _id: 0
                                }
                            }
                        ],
                        as: 'segments.ledgerAccount'
                    }}, {$unwind: {
                        path: "$segments.ledgerAccount",
                        preserveNullAndEmptyArrays: false
                    }}, {$group: {
                        _id: {_id: "$_id",
                            name: "$name",
                            ruleGroups: "$ruleGroups",
                            providerGroups: "$providerGroups",
                        },
                        segments: {
                            $push: "$segments"
                        }
                    }}*/, {
                    $lookup: {
                        from: 'ledgeringRuleGroups',
                        localField: 'ruleGroups',
                        foreignField: 'name',
                        as: 'ruleGroups'
                    }
                }, {
                    $lookup: {
                        from: 'ledgeringProviderGroups',
                        localField: 'providerGroups',
                        foreignField: 'name',
                        as: 'providerGroups'
                    }
                }, {
                    $project: {
                        _id: 0,
                        name: "$name",
                        ruleGroups: "$ruleGroups",
                        providerGroups: "$providerGroups",
                        segments: "$segments",
                    }
                }, {
                    $project: {
                        "ruleGroups._id": 0,
                        "providerGroups._id": 0
                    }
                }]
            );
            return await aggregation.next();
        } catch (e) {
            console.error(`Unable to get ledgeringFrame(${name}): ${e}`);
            throw e;
        }
    }
}