let ledgeringProviderGroups

export default class LedgeringProviderGroupsDao {

    static async injectDB(conn) {
        if (ledgeringProviderGroups) {
            return
        }
        try {
            ledgeringProviderGroups = await conn.db(process.env.DB_NS).collection("ledgeringProviderGroups");
            await ledgeringProviderGroups.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertProviderGroups(providerGroups) {
        try {
            return await ledgeringProviderGroups.insertMany(providerGroups)
                .then(result => {
                    return {ledgeringProviderGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ledgeringProviderGroups: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ledgeringProviderGroups.deleteMany()
                .then(result => {
                    return {ledgeringProviderGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ledgeringProviderGroups: ${e}`);
            throw e;
        }
    }

}