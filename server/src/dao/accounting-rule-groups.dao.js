let accountingRuleGroups

export default class AccountingRuleGroupsDao {

    static async injectDB(conn) {
        if (accountingRuleGroups) {
            return
        }
        try {
            accountingRuleGroups = await conn.db(process.env.DB_NS).collection("accountingRuleGroups");
            await accountingRuleGroups.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertRuleGroups(ruleGroups) {
        try {
            return await accountingRuleGroups.insertMany(ruleGroups)
                .then(result => {
                    return {accountingRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post accountingRuleGroups: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await accountingRuleGroups.deleteMany()
                .then(result => {
                    return {accountingRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete accountingRuleGroups: ${e}`);
            throw e;
        }
    }

}