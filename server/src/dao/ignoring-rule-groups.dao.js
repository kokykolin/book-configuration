let ignoringRuleGroups

export default class IgnoringRuleGroupsDao {

    static async injectDB(conn) {
        if (ignoringRuleGroups) {
            return
        }
        try {
            ignoringRuleGroups = await conn.db(process.env.DB_NS).collection("ignoringRuleGroups");
            await ignoringRuleGroups.createIndex({name: 1}, {unique: true});
        } catch (e) {
            console.error(`Unable to establish collection handles in ${__filename}: ${e}`)
        }
    }

    static async insertRuleGroups(ruleGroups) {
        try {
            return await ignoringRuleGroups.insertMany(ruleGroups)
                .then(result => {
                    return {ignoringRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to post ignoringRuleGroups: ${e}`);
            throw e;
        }
    }

    static async deleteAll() {
        try {
            return await ignoringRuleGroups.deleteMany()
                .then(result => {
                    return {ignoringRuleGroups: result.result.n}
                });
        } catch (e) {
            console.error(`Unable to delete ignoringRuleGroups: ${e}`);
            throw e;
        }
    }

}