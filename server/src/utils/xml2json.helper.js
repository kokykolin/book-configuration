import _ from "lodash";

export const frameMapper = (frame) => {
    const ruleGroups = Array.isArray(frame.ruleGroups.group) ?
        frame.ruleGroups.group.map(group => group.name) : [frame.ruleGroups.group.name];
    const providerGroups =
        frame.providerGroups === undefined ? [] :
        Array.isArray(frame.providerGroups.group) ?
        frame.providerGroups.group.map(group => group.name) : [frame.providerGroups.group.name];
    const segments =
        frame.segments === undefined ? [] :
        Array.isArray(frame.segments.segment) ? frame.segments.segment : [frame.segments.segment];
    return {name: frame.name, ruleGroups, providerGroups, segments};
};

export const providerGroupMapper = (providerGroup) => {
    const accounting =
        providerGroup.accounting === undefined ? undefined :
            Array.isArray(providerGroup.accounting.provider) ?
                providerGroup.accounting.provider.map(provider => provider.type) : [providerGroup.accounting.provider.type];
    const ranking =
        providerGroup.ranking === undefined ? undefined :
            Array.isArray(providerGroup.ranking.provider) ?
                providerGroup.ranking.provider.map(provider => provider.type) : [providerGroup.ranking.provider.type];
    const ownership =
        providerGroup.ownership === undefined ? undefined :
            Array.isArray(providerGroup.ownership.provider) ?
                providerGroup.ownership.provider.map(provider => provider.type) : [providerGroup.ownership.provider.type];
    const ledgering =
        providerGroup.ledgering === undefined ? undefined :
            Array.isArray(providerGroup.ledgering.provider) ?
                providerGroup.ledgering.provider.map(provider => provider.type) : [providerGroup.ledgering.provider.type];
    return _.pickBy({name: providerGroup.name, accounting, ranking, ownership, ledgering},  _.identity);
};

export const ruleGroupMapper = (ruleGroup) => {
    const rules = Array.isArray(ruleGroup.rule) ?
        ruleGroup.rule : [ruleGroup.rule];
    return {name: ruleGroup.name, rules};
};
