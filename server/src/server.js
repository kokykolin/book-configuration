import express from "express"
import expressWs from "express-ws";
import bodyParser from "body-parser"
import busboy from "connect-busboy";
import {MongoClient} from "mongodb";
import cors from "cors";

import configuration from "./api/configuration.route"
import ledgering from "./api/ledgering.route"
import AccountingRuleGroupsDao from "./dao/accounting-rule-groups.dao";
import AccountingProviderGroupsDao from "./dao/accounting-provider-groups.dao";
import AccountingFramesDao from "./dao/accounting-frames.dao";
import LedgeringAccountsDao from "./dao/ledgering-accounts.dao";
import LedgeringRuleGroupsDao from "./dao/ledgering-rule-groups.dao";
import LedgeringProviderGroupsDao from "./dao/ledgering-provider-groups.dao";
import LedgeringFramesDao from "./dao/ledgering-frames.dao";
import IgnoringFramesDAO from "./dao/ingnoring-frames.dao";
import IgnoringRuleGroupsDao from "./dao/ignoring-rule-groups.dao";

const port = process.env.PORT || 8000;

const {app} = expressWs(express());
app.use(busboy());
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use("/api/configuration", configuration);
app.use("/api/ledgering", ledgering);
app.use("/status", express.static("client"));
app.use("/", express.static("client"));
app.use("*", (req, res) => res.status(404).json({error: "not found"}));

app.use((err, req, res, next) => {
    res.status(500)
        .json({message: err.message});
    next(err);
});

MongoClient.connect(
    process.env.DB_URI,
    {poolSize: 50, wtimeout: 2500, useUnifiedTopology: true},
)
    .catch(err => {
        console.error(err.stack);
        process.exit(1);
    })
    .then(async client => {
        await AccountingRuleGroupsDao.injectDB(client);
        await AccountingProviderGroupsDao.injectDB(client);
        await AccountingFramesDao.injectDB(client);
        await LedgeringAccountsDao.injectDB(client);
        await LedgeringRuleGroupsDao.injectDB(client);
        await LedgeringProviderGroupsDao.injectDB(client);
        await LedgeringFramesDao.injectDB(client);
        await IgnoringRuleGroupsDao.injectDB(client);
        await IgnoringFramesDAO.injectDB(client);
        app.listen(port, () => {
            console.log(`listening on port ${port}`)
        })
    });