import React, {Component} from "react";

export default function withWebSocketClient(path, WrappedComponent) {
    return class extends Component {
        constructor(props) {
            super(props);
            this.state = {ws: null};
        }

        componentDidMount() {
            this.connect();
        }

        timeout = 250;
        connect = () => {
            const ws = new WebSocket(`ws://${process.env.REACT_APP_HOST_URL}/api${path}`);
            let that = this;
            let connectInterval;

            ws.onopen = () => {
                console.log("Connected websocket main component");
                this.setState({ws: ws});
                that.timeout = 250;
                clearTimeout(connectInterval);
            };

            ws.onclose = e => {
                console.log(
                    `Socket is closed. Reconnect will be attempted in ${Math.min(
                        10000 / 1000,
                        (that.timeout + that.timeout) / 1000
                    )} second.`,
                    e
                );

                that.timeout = that.timeout + that.timeout;
                connectInterval = setTimeout(this.check, Math.min(10000, that.timeout));
            };

            ws.onerror = error => {
                console.error(
                    "Socket encountered error: ",
                    error.message,
                    "Closing socket"
                );

                ws.close();
            };
        };

        check = () => {
            const {ws} = this.state;
            if (!ws || ws.readyState === WebSocket.CLOSED) this.connect();
        };

        render() {
            return <WrappedComponent
                {...this.props}
                ws={this.state.ws}
            />
        }
    }
}
