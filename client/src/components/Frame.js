import React from 'react';
import {Label, List, Segment} from "semantic-ui-react";
import _ from 'lodash';
import {connect} from "react-redux";
import {loadFrame, openFrame} from "../actions";
import ReactJson from "react-json-view";
import "./Frame.css";

class Frame extends React.Component {

    handleClick(e) {
        e.stopPropagation();
        if (!this.props.frame.meta.loaded) {
            this.props.loadFrame(this.props.name);
        } else {
            this.props.openFrame(this.props.name, !this.props.frame.meta.open);
        }
    }

    render() {
        return <List.Item className={`frame ${this.props.frame.meta.open ? 'open' : ''}`} onClick={(e) => this.handleClick(e)}>
            <Label color='blue' horizontal>
                {this.props.number+1}
            </Label>
            <b>{this.props.name}</b>
            {this.props.frame.meta.open && <List.Content><Segment onClick={(e) => e.stopPropagation()}>
                <ReactJson src={_.omit(this.props.frame, ['meta'])}/>
            </Segment></List.Content>}
        </List.Item>
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        frame: state.frames[ownProps.name]
    };
};

export default connect(
    mapStateToProps,
    {loadFrame, openFrame}
)(Frame);
