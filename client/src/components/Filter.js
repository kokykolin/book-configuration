import * as React from "react";
import {Checkbox, Dropdown, Form, Label} from "semantic-ui-react";
import {connect} from 'react-redux';
import {Field, reduxForm} from "redux-form";
import _ from "lodash";
import {filterAccounts, loadAccounts, loadFrames, submitFilter} from "../actions";
import "./Filter.css";

class Filter extends React.Component {

    componentDidMount() {
        this.props.loadAccounts();
    }

    renderSelect = ({input, label}) => {
        input.value = input.value || [];
        return (
            <Form.Field>
                <Dropdown
                    {...input}
                    onChange={(param, data) => input.onChange(data.value)}
                    onBlur={(param, data) => input.onBlur(data.value)}
                    placeholder={label}
                    multiple
                    search
                    selection
                    loading={this.props.isLoading}
                    options={this.suggestedAccounts()}
                />
            </Form.Field>
        );
    };

    renderCheckbox = ({input, label, disabled}) => {
        return (
            <Form.Field>
                <Checkbox {..._.omit(input, ['value'])} label={label}
                          onChange={(e, {checked}) => input.onChange(checked)}
                          disabled={disabled}
                />
            </Form.Field>
        );
    }

    suggestedAccounts() {
        return this.props.accounts
            .filter(account => account.filtered)
            .map(account => {
                return {key: `${account.account}`, value: `${account.account}`, text: `${account.account}`}
            });
    }

    renderCheckboxGroup(attribute, label) {
        const checkboxes = (this.props.filter[attribute] || [])
            .sort((item1, item2) => item1.value <= item2.value ? -1 : 1)
            .map(item =>
                <Field key={item.value}
                       name={`${attribute}.${item.value}`}
                       label={item.value}
                       component={this.renderCheckbox}
                       disabled={!item.enabled}/>);

        return (
            !_.isEmpty(this.props.filter[attribute]) &&
            <React.Fragment>
                <h4 className="ui inverted dividing header">{label}</h4>
                {checkboxes}
            </React.Fragment>
        );
    }

    render() {
        return (
            <React.Fragment>
                <h4 className="ui top inverted attached header">
                    Filter ledgering frames
                </h4>
                <div className="ui inverted attached segment">
                    <form className="ui inverted form">
                        <h4 className="ui inverted dividing header">Account number<Label circular
                                                                                         size='small'>{this.props.filteredCount}</Label>
                        </h4>
                        <Field name="accountNumbers"
                               component={this.renderSelect}
                               label='Account number'/>
                        {this.renderCheckboxGroup('accountOwners', 'Account owner')}
                        {this.renderCheckboxGroup('schemes', 'Account scheme')}
                        <h4 className="ui inverted dividing header">Segment side</h4>
                        <Field name="sides.CREDIT" label="CREDIT" component={this.renderCheckbox}/>
                        <Field name="sides.DEBIT" label="DEBIT" component={this.renderCheckbox}/>
                    </form>
                </div>
            </React.Fragment>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...state.ledgeringFilter
    };
};

const formWrapped = reduxForm({
    form: 'filterLedgeringFrames',
    onChange: (values, dispatch, props) => {
        dispatch(submitFilter(values, props.ws));
    }
})(Filter);

export default connect(
    mapStateToProps,
    {loadAccounts, filterAccounts, submitFilter, loadFrames}
)(formWrapped);