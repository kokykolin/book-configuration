import React from 'react';
import {Container, Grid, Icon, Menu, Ref, Sidebar, Sticky} from "semantic-ui-react";
import Filter from "./Filter";
import Frames from "./Frames";
import "./App.css";

import withWindowDimensions from "./WindowDimensions";
import {connect} from "react-redux";
import {toggleMenu} from "../actions";
import withWebSocketClient from "./WebSocketConnector";


class App extends React.Component {

    contextRef = React.createRef();

    componentDidMount() {
        if (!this.props.isComputer) {
            this.props.toggleMenu(false);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isComputer !== this.props.isComputer && prevProps.isComputer && prevProps.showMenu) {
            this.props.toggleMenu(false);
        } else if (prevProps.isComputer !== this.props.isComputer && this.props.isComputer) {
            this.props.toggleMenu(true);
        }
    }

    render() {
        return (
            <div className="ui fluid container">
                {this._stickyTopMenuBar()}
                {this._stickySideBar()}
                <Sidebar.Pusher>
                    <Container id='content' className={!this.props.showMenu ? 'side-menu-hidden' : ''}>
                        <Ref innerRef={this.contextRef}>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column>
                                        <h1 className="ui left aligned header">BookConfigurator3000</h1>
                                    </Grid.Column>
                                </Grid.Row>
                                <Grid.Row>
                                    <Grid.Column mobile={16} tablet={16} computer={11} largeScreen={9} widescreen={8}>
                                        <Frames ws={this.props.ws}/>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Ref>
                    </Container>
                </Sidebar.Pusher>
            </div>
        );
    }

    _stickyTopMenuBar() {
        return (
            <Sticky context={this.contextRef} id="mobile-sticky">
                <Menu
                    icon
                    inverted
                    attached='top'>
                    <Menu.Item
                        onClick={() => this.props.toggleMenu(!this.props.showMenu)}>
                        <Icon name='content'/>
                    </Menu.Item>
                </Menu>
            </Sticky>
        );
    }

    _stickySideBar() {
        return (
            <Sticky context={this.contextRef}>
                <Sidebar
                    as={Menu}
                    direction='left'
                    inverted
                    vertical
                    visible={this.props.isComputer ? true : this.props.showMenu}
                    onHide={!this.props.isComputer ? () => this.props.toggleMenu(false) : undefined}
                    target={this.contextRef}
                    id='menu'>
                    <Filter ws={this.props.ws}/>
                </Sidebar>
            </Sticky>
        );
    }
};

const mapStateToProps = (state, ownProps) => {
    return {
        showMenu: state.showMenu
    };
};

export default connect(
    mapStateToProps,
    {toggleMenu}
)(withWebSocketClient('/ledgering/frames', withWindowDimensions(App)));
