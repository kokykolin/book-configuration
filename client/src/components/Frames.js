import * as React from "react";
import Frame from "./Frame";
import {Grid, Label, List, Segment} from "semantic-ui-react";
import {loadFrames, submitFilter} from "../actions";
import {connect} from "react-redux";
import _ from "lodash";

class Frames extends React.Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.ws && !prevProps.ws) {
            this.props.loadFrames({}, this.props.ws);
        }
    }

    renderFrames() {
        let number = 0;
        return _.keys(this.props.frames)
            .map(frame =>
                <Frame name={frame} key={frame} label='LedgeringFrame' labelprops={{color: 'red'}} number={number++}/>
            );
    }

    render() {
        return <Grid>
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Label>Frames<Label.Detail>{_.keys(this.props.frames).length}</Label.Detail></Label>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Segment>
                        <List divided selection>
                            {this.renderFrames()}
                        </List>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    }
}

const mapStateToProps = state => {
    return {
        frames: state.frames
    };
};

export default connect(
    mapStateToProps,
    {loadFrames, submitFilter}
)(Frames);

