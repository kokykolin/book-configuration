import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import {
    ADD_TO_FRAME_LIST,
    CLEAR_FRAME_LIST,
    LOAD_ACCOUNT_LIST,
    LOAD_FILTER,
    LOAD_FRAME,
    LOADING_FORM_DATA,
    SHOW_MENU
} from "../actions";

const INIT_LEDGERING_FILTER_STATE = {
    accounts: [],
    filter: {},
    isLoading: false,
    filteredCount: 0
};

const ledgeringFilter = (state = INIT_LEDGERING_FILTER_STATE, action) => {
    switch (action.type) {
        case LOAD_ACCOUNT_LIST:
            const filteredCount = action.payload.filter(account => account.filtered).length;
            return {...state, accounts: [...action.payload], filteredCount};
        case LOADING_FORM_DATA:
            return {...state, isLoading: action.payload};
        case LOAD_FILTER:
            return {...state, filter: {...action.payload}, fake: Math.random()}; //TODO why God, why?
        default:
            return state
    }
};

const frames = (state = {}, action) => {
    if (action.type === ADD_TO_FRAME_LIST) {
        return action.payload.reduce((currentState, frame) => {
           return {...currentState, [frame.name]: frame}
        }, state);
    } else if (action.type === CLEAR_FRAME_LIST) {
        return {};
    } else if (action.type === LOAD_FRAME) {
        state[action.payload.name] = action.payload;
        return {...state};
    }
    return state;
};

const showMenu = (state = false, action) => {
    if (action.type === SHOW_MENU) {
        return action.payload;
    }
    return state;
};

export default combineReducers({
    form: formReducer,
    showMenu,
    frames,
    ledgeringFilter
});
