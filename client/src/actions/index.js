import confManager from "../api/confManager";
import _ from "lodash";

export const LOADING_FORM_DATA = 'LOADING_FORM_DATA';
export const LOADING_FRAME_LIST = 'LOADING_FRAME_LIST';
export const LOAD_FRAME = 'LOAD_FRAME';
export const ADD_TO_FRAME_LIST = 'ADD_TO_FRAME_LIST';
export const CLEAR_FRAME_LIST = 'CLEAR_FRAME_LIST';
export const LOAD_ACCOUNT_LIST = 'LOAD_ACCOUNT_LIST';
export const LOAD_FILTER = 'LOAD_FILTER';
export const SHOW_MENU = 'SHOW_MENU';

export const loadAccounts = () => async (dispatch) => {
    dispatch({type: LOADING_FORM_DATA, payload: true});
    const response = await confManager.get('/ledgering/accounts', {});
    response.data.forEach(account => {
            account.filtered = true;
        }
    );
    dispatch({type: LOAD_ACCOUNT_LIST, payload: response.data});
    dispatch(loadFilter({}));
    dispatch({type: LOADING_FORM_DATA, payload: false});
};

export const loadFrame = (name) => async dispatch => {
    const response = await confManager.get('/ledgering/frame', {params:{name}});
    dispatch({type: LOAD_FRAME, payload: {...response.data, meta: {loaded: true, open: true}}});
};

export const openFrame = (name, value) => (dispatch, getState) => {
    const frame = getState().frames[name];
    frame.meta.open = value;
    dispatch({type: LOAD_FRAME, payload: {...frame}});
};

export const loadFilter = (formValues = {}) => async (dispatch) => {
    const request = mapValuesToRequest(formValues);
    const response = await confManager.get(
        '/ledgering/filters',  {params: request});
    dispatch({type: LOAD_FILTER, payload: response.data});
};

export const loadFrames = (formValues, ws) => async (dispatch) => {
    dispatch({type: LOADING_FRAME_LIST, payload: true});
    dispatch({type: CLEAR_FRAME_LIST, payload: []});
    const request = mapValuesToRequest(formValues);
    const reqId = `${Math.random()}`;
    ws.send(JSON.stringify({...request, reqId}));
    ws.onmessage = async data => {
        const msgObject = JSON.parse(data.data);
        if (reqId === msgObject.reqId) {
            const frames = _.chain(msgObject.data)
                .map(frame => {
                    return {..._.pick(frame, 'name'), meta: {}}
                })
                .value();
            await dispatch({type: ADD_TO_FRAME_LIST, payload: frames});
        }
    };
    dispatch({type: LOADING_FRAME_LIST, payload: false});
};

export const submitFilter = (formValues, ws) => async (dispatch) => {
    await dispatch(filterAccounts(formValues));
    await dispatch(loadFrames(formValues, ws));
};

export const filterAccounts = formValues => (dispatch, getState) => {
    const {ledgeringFilter} = getState();

    extendValuesWithFalse(ledgeringFilter.filter, formValues);
    ledgeringFilter.accounts.map(account => setFilterByValues(account, formValues));
    dispatch({type: LOAD_ACCOUNT_LIST, payload: [...ledgeringFilter.accounts]});

    dispatch(loadFilter(formValues));

   /* formValues.accountNumbers = _.isEmpty(formValues.accountNumbers) ? [] : formValues.accountNumbers;
    const selectedAccounts = ledgeringFilter.accounts.filter(account => formValues.accountNumbers.includes(account.account));

    enableFilterByAccounts(['accountOwners', 'schemes'], ledgeringFilter.filter, selectedAccounts);
    dispatch({type: LOAD_FILTER, payload: {...ledgeringFilter.filter}});*/
};

export const toggleMenu = (state) => {
    return {type: SHOW_MENU, payload: state}
};



/*
const enableFilterByAccounts = (fields, filter, accounts) => {
    fields.forEach(field => {
            const filteredByAccounts = _.chain(accounts)
                .map(field)
                .flatten()
                .uniq()
                .value();

            filter[field].forEach(item => {
                if (filteredByAccounts.includes(item.value) || _.isEmpty(filteredByAccounts)) {
                    item.enabled = true;
                } else {
                    item.enabled = false;
                }
            });

    });
};
*/
const mapValuesToRequest = ({accountNumbers, accountOwners, schemes, sides}) => {
    return {
        accounts: accountNumbers || [],
        accountOwners: checkedValues(accountOwners),
        schemes: checkedValues(schemes),
        sides: checkedValues(sides)
    };
};

const checkedValues = (object = {}) => {
    return _.toPairs(object).filter(([key, value]) => value).map(([key, value]) => key);
};

const setFilterByValues = (account, formValues) => {
    account.filtered = _.toPairs(formValues).reduce((total, pair) => filterByField(total, pair, account), true);
    return account;
};

const filterByField = (total, pair, account) => {
    const [key, value] = pair;
    if (_.values(value).every(v => v === false) || _.values(value).every(v => v === true)) {
        return total && true;
    }
    if (!_.keys(account).includes(key)) {
        return total && true;
    }
    return total &&
        _.intersection(
        _.toPairs(value)
        .filter(([key, value]) => value)
        .map(([key, value]) => key),
        account[key]).length;
};

const extendValuesWithFalse = (filter, formValues) => {
    _.keys(filter).forEach(field =>
        extendFormValues('accountOwners', filter, formValues)
    );
    _.keys(filter).forEach(field =>
        extendFormValues('schemes', filter, formValues)
    );
};

const extendFormValues = (field, filter, formValues) => {
    formValues[field] = _.assignIn(
        ...filter[field].map(owner => {
            return {[owner.value]: false}
        }),
        {...formValues[field]});
};
